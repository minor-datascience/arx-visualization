"""
Convert tree format1 to tree format2 for this tool: https://lautgesetz.com/latreex/

Format1-----------------
United-States;North America;*
Cambodia;Asia;*
England;Europe;*
Puerto-Rico;North America;*

Format2-----------------
*
-North America
--United-States
--Puerto-Rico
--Canada
-Asia
--Cambodia
--India
"""
FILENAME = "country.csv"  # filename with format1
OUTPUT_FILENAME = "taxonomy-trees/" + FILENAME  # filename for output format2

file = open("taxonomy-csvs/" + FILENAME)

lines = [list(reversed(line.strip().split(";"))) for line in file]
print("Lines:", lines)


def fill_with_children(root, level=0):
    """
    result = {"level": 0, "name": "*", "children": [{"level": 1, "name": "Europe", "children": []}]}
    """

    uniques = []
    for value_list in lines:
        # stop if depth reached
        if len(value_list) < level + 2:
            return root

        # get child value
        value = value_list[level + 1]

        # dont add child values which dont belong to this root
        root_value = value_list[level]
        if root_value != root["name"]:
            continue

        if value not in uniques:
            uniques.append(value)
    for unique in uniques:
        dic = {"name": unique, "level": level + 1, "children": []}
        fill_with_children(dic, level + 1)
        root["children"].append(dic)

    return root


root = {"name": "*", "level": 0, "children": []}
result = fill_with_children(root)
print("Result:", result)

# print output to file
file = open(OUTPUT_FILENAME, "w")
file.truncate(0)


def print_childs(root, level=1):
    for child in root["children"]:
        print(level * "-" + child["name"], file=file)

        if "children" in root:
            print_childs(child, level + 1)


print(result["name"], file=file)
print_childs(result)
print("Done writing result to file.")
