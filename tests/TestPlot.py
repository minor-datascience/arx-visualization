import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

SAVE_FOLDER = "plots-no-k1/"
SOURCE_FOLDER = "plotcsvs/"
filename = "RESULT_classification_accuracy.csv"
df = pd.read_csv(SOURCE_FOLDER + filename, index_col=False)

df = df.drop(df.columns[0], axis=1)
df = df.T
plot = sns.lineplot(data=df)
plt.legend(loc="upper right", labels=["A coarse", "A detailed", "B coarse", "B detailed"])
plt.title("Classification accuracy")
plt.xlabel("K-value")
plt.ylabel("Percentage")
plt.show()
