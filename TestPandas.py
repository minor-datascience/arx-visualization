import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

SAVE_FOLDER = "plots-no-k1/"
SOURCE_FOLDER = "plot-csvs/"
utility_file = "classification_accuracy.csv"
disclosure_file = "highest_risk.csv"
utility_df = pd.read_csv(SOURCE_FOLDER + utility_file, index_col=False)
disclosure_df = pd.read_csv(SOURCE_FOLDER + disclosure_file, index_col=False)

kkk = [int(k) for k in utility_df.columns]
print(kkk)
