import pandas as pd
from itertools import *

df = pd.read_csv("bdetailedk10.csv")

groups = ["age", "workclass", "education", "marital-status", "occupation", "relationship",
          "race", "sex", "capital-gain", "capital-loss", "hours-per-week", "native-country", "salary"]

qids = ["sex", "race", "age", "capital-gain", "capital-loss", "hours-per-week"]

unique_rows = df.groupby(groups).size().reset_index().rename(columns={0: "count"})

unique_rows = unique_rows[unique_rows["age"] != "*"]
unique_rows = unique_rows[unique_rows["count"] == 1]

combs = [list(combinations(df.columns, x)) for x in range(2, len(df.columns) + 1)]

print(len(df))
print(len(unique_rows))

results = []

iterated = 0
total_rows = len(df)

for index, row in df.iterrows():
    try:
        iterated += 1
        print(f"Iterated {iterated}/{total_rows}")

        for combination in combs:
            for keys in combination:
                # create dict with each key=value of the row {'age': '40', 'workclass': 'Government', etc}
                filter_dict = {key: row[key] for key in keys}

                # dataframe with the dict applied as filter (idk how it works)
                filtered_df = df.loc[(df[list(filter_dict)] == pd.Series(filter_dict)).all(axis=1)]

                amount_of_others = len(filtered_df)  # amount of records that have the conditions in the dict

                # save if this combination makes the record unique
                if amount_of_others == 1:
                    smallest_combination = len(filter_dict)
                    # print(f"Found smallest combination of {smallest_combination}")
                    result = {"filter": filter_dict,
                              "smallest_combination": smallest_combination,
                              "row": row,
                              "index": index
                              }
                    results.append(result)
                    break
    except Exception as e:
        print(f"Oopsie woopsie: {e}")

with open("results.csv", "w") as file:
    sorted_results = sorted(results, key=lambda k: k["smallest_combination"])
    for sorted_result in sorted_results:
        print(sorted_result, file=file)

print(f"Done: {len(sorted_results)} results in {len(df)}")
