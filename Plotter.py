"""
Plot CSVs and save plots in specified folders.
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

SOURCE_FOLDER = "plot-csvs/"
SAVE_FOLDER = "plots/"


def plot_thing(filename, title, legend_location, drop_first_column=False, decimals=0):
    print(f"Plotting {filename} with title {title} with legend at {legend_location} "
          f"{'with dropping first column' if drop_first_column else ''}.")
    df = pd.read_csv(SOURCE_FOLDER + filename, index_col=False)

    if drop_first_column:
        df = df.drop(df.columns[0], axis=1)
    df = df.T

    ax = sns.lineplot(data=df)
    ax.yaxis.set_major_formatter(mtick.PercentFormatter(decimals=decimals))
    plt.legend(loc=legend_location, labels=["A coarse", "A detailed", "B coarse", "B detailed"])
    # plt.title(title)
    plt.xlabel("K-value")
    plt.ylabel(title)
    plt.show()

    ax.get_figure().savefig(f"{SAVE_FOLDER}{filename[:-4]}.png", format="png", dpi=300)
    ax.get_figure().savefig(f"{SAVE_FOLDER}{filename[:-4]}.svg", format="svg", dpi=1200)
    ax.get_figure().savefig(f"{SAVE_FOLDER}{filename[:-4]}.pdf", format="pdf", dpi=1200)


# write plots with K1 included
plot_thing("classification_accuracy.csv", "Anonimized classification accuracy", "lower right")
plot_thing("granularity.csv", "Granularity", "lower left")
plot_thing("discernibility.csv", "Discernibility", "lower left")
plot_thing("estimated_prosecuter_risk.csv", "Estimated prosecuter risk", "upper right")
plot_thing("lowest_risk.csv", "Records affected by lowest risk", "upper left")
plot_thing("highest_risk.csv", "Records affected by highest risk", "upper right")
print(f"Done writing plots to {SAVE_FOLDER}")

# write plots without K1
SAVE_FOLDER = "plots-no-k1/"
plot_thing("classification_accuracy.csv", "Anonimized classification accuracy", "lower left", True)
plot_thing("granularity.csv", "Granularity", "lower left", True)
plot_thing("discernibility.csv", "Discernibility", "lower left", True)
plot_thing("estimated_prosecuter_risk.csv", "Estimated prosecuter risk", "upper right", True, 1)
plot_thing("lowest_risk.csv", "Records affected by lowest risk", "upper left", True)
plot_thing("highest_risk.csv", "Records affected by highest risk", "lower right", True, 1)

print(f"Done writing plots to {SAVE_FOLDER}")
