import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

SOURCE_FOLDER = "plot-csvs/"
SAVE_FOLDER = "ud-plots/"


def go(output_filename, utility_filename, disclosure_filename, row_index, x_label="Utility", y_label="Disclosure"):
    utility_df = pd.read_csv(SOURCE_FOLDER + utility_filename, index_col=False)
    disclosure_df = pd.read_csv(SOURCE_FOLDER + disclosure_filename, index_col=False)

    kk = [int(k) for k in utility_df.columns]  # all k values

    to_drop = [0, 1, 2, 3]
    to_drop.remove(row_index)
    utility_df = utility_df.drop(to_drop)
    print(utility_df.head())
    utility_df = pd.melt(utility_df)
    x = [x for x in utility_df["value"]]
    y = [x for x in pd.melt(disclosure_df.drop(to_drop))["value"]]

    # drop k1
    x.pop(0)
    y.pop(0)
    kk.pop(0)

    print(x)
    print(y)

    ax = sns.scatterplot(x=x, y=y)

    # ax.yaxis.set_major_formatter(mtick.PercentFormatter(decimals=2))
    # ax.xaxis.set_major_formatter(mtick.PercentFormatter(decimals=0))

    ax.yaxis.set_major_formatter(mtick.PercentFormatter(decimals=1))
    ax.xaxis.set_major_formatter(mtick.PercentFormatter(decimals=1))

    plt.gca().invert_yaxis()  # flip y axis because lower y values are better, so they should be higher in the graph

    # put K value at each data point
    margin = 0.05
    for i, txt in enumerate(kk):
        plt.annotate(text=f"k-{txt}", xy=(x[i] + margin, y[i] + 0.3), size=10)
        # plt.annotate(txt, (x[i] + margin, y[i] + 0.2))

    ax.set(xlim=(77, 82))
    # plt.title(f"{x_label} compared to {y_label} for all K values")
    # plt.title(f"Utility-privacy trade-off analysis for all values of K")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.show()
    output_filename = f"{SAVE_FOLDER}{output_filename}"
    ax.get_figure().savefig(f"{output_filename}.png", format="png", dpi=300)
    ax.get_figure().savefig(f"{output_filename}.svg", format="svg", dpi=1200)
    ax.get_figure().savefig(f"{output_filename}.pdf", format="pdf", dpi=1200)
    plt.clf()


go("a-class-prosrisk","classification_accuracy.csv", "estimated_prosecuter_risk.csv", 1, "Classification accuracy", "Estimated prosecuter risk")
go("b-class-prosrisk","classification_accuracy.csv", "estimated_prosecuter_risk.csv", 3, "Classification accuracy", "Estimated prosecuter risk")

# utility_files = ["classification_accuracy.csv", "discernibility.csv", "granularity.csv"]
# disclosure_files = ["estimated_prosecuter_risk.csv", "lowest_risk.csv", "highest_risk.csv"]
# for utility_file in utility_files:
#     for disclosure_file in disclosure_files:
#         x_label = utility_file.replace("_", " ").capitalize()[:-4]
#         y_label = disclosure_file.replace("_", " ").capitalize()[:-4]
#         go(utility_file, disclosure_file, 3, x_label, y_label)

# disclosure_file = "highest_risk.csv"
# go(utility_file, disclosure_file, 1, "Classification accuracy", "Highest risk")
