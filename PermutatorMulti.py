from multiprocessing.pool import ThreadPool

import pandas as pd
import multiprocessing
from functools import partial
from contextlib import contextmanager
from itertools import *

df = pd.read_csv("bdetailedk10.csv")

groups = ["age", "workclass", "education", "marital-status", "occupation", "relationship",
          "race", "sex", "capital-gain", "capital-loss", "hours-per-week", "native-country", "salary"]

qids = ["sex", "race", "age", "capital-gain", "capital-loss", "hours-per-week"]

unique_rows = df.groupby(groups).size().reset_index().rename(columns={0: "count"})

unique_rows = unique_rows[unique_rows["age"] != "*"]
unique_rows = unique_rows[unique_rows["count"] == 1]

combs = [list(combinations(df.columns, x)) for x in range(2, len(df.columns) + 1)]

print(len(df))
print(len(unique_rows))

results = []

iterated = 0


def go(input):
    try:
        (index, row) = input

        global iterated
        iterated += 1
        if iterated % 100 == 0:
            print(f"Iterated {iterated}")
        print(iterated)

        for combination in combs:
            for keys in combination:
                # create dict with each key=value of the row {'age': '40', 'workclass': 'Government', etc}
                filter_dict = {key: row[key] for key in keys}

                # dataframe with the dict applied as filter (idk how it works)
                filtered_df = df.loc[(df[list(filter_dict)] == pd.Series(filter_dict)).all(axis=1)]

                amount_of_others = len(filtered_df)  # amount of records that have the conditions in the dict

                # save if this combination makes the record unique
                if amount_of_others == 1:
                    smallest_combination = len(filter_dict)
                    # print(f"Found smallest combination of {smallest_combination}")
                    global result
                    result = {"filter": filter_dict,
                              "smallest_combination": smallest_combination,
                              "row": row,
                              "index": index
                              }
                    results.append(result)
                    break
    except Exception as e:
        print(f"Oopsie woopsie: {e}")


print("Loading dataframe in list.")
row_list = [(index, row) for index, row in df.iterrows()]
print("Start searching.")

@contextmanager
def poolcontext(*args, **kwargs):
    pool = multiprocessing.Pool(*args, **kwargs)
    yield pool
    pool.terminate()


if __name__ == '__main__':
    with poolcontext(processes=6) as pool:
        pool.map(go, row_list)

    print(results)

    print("Printing results.")
    count = 0
    sorted_results = sorted(results, key=lambda k: k["smallest_combination"])
    for sorted_result in sorted_results:
        print(sorted_result["smallest_combination"])
        print(sorted_result["filter"])
        count += 1
        if count > 1:
            break

    print(f"Done: {len(sorted_results)} results in {len(df)}")
