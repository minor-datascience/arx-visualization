"""
Convert all Google Sheets exports found in sheetsexports folder to CSVs, and write them
in plot-csvs folder, overwriting existing files.
"""
import os


def convert_to_csv(dir_entry):
    input_file = open(dir_entry.path)
    output_path = "plot-csvs/" + dir_entry.name
    output_file = open(output_path, mode="w")

    output_file.truncate(0)
    for input_line in input_file:
        values = input_line.split()  # split values by whitespace
        output_line = ""
        for value in values:
            output_line += value + ","
        output_line = output_line[:-1]  # remove trailing comma
        print(output_line, file=output_file)

    print("Wrote result to", output_path)


for dir_entry in os.scandir("sheetsexports"):
    convert_to_csv(dir_entry)
